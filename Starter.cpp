//
// Created by dragoon on 10/21/17.
//

#include "Starter.h"
#include "Runner.h"

int Starter::runProc(int argc, char **argv) {
    if (argc == 4) {
        Runner runner(argv[1], argv[2], argv[3]);
        runner.run();
    } else {
        Runner runner("127.0.0.1", "31001", "0000000000000000");
        runner.run();
    }
    return 0;
}