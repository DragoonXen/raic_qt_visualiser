#ifndef CG_MAINWINDOW_H
#define CG_MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QSlider>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QGraphicsView>
#include "MyStrategy.h"
#include "model/World.h"
#include "TickData.h"
#include "CoordDrawingHelper.h"
#include "DrawingData.h"
#include "DrawPanel.h"
#include "TextDrawingPanel.h"
#include "FpsMeasurer.h"
#include <mutex>
#include <model/PlayerContext.h>
#include <condition_variable>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
Q_OBJECT
private:
    static const std::array<int, 11> framesPerSecond;

public:
    static MainWindow *getInstance();

    explicit MainWindow(QWidget *parent = 0);

    void initConstants(model::Game game, size_t teamSize);

    ~MainWindow();

    void prepareTickData(const model::PlayerContext &context);

    void updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy);

    void finishTick();

private slots:

    void runRepaint();

    void changeDrawingTick(int changedValue);

protected:

    void resizeEvent(QResizeEvent *evt) override;

    void keyPressEvent(QKeyEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;

private:
    int millisToNextDraw; // 30 fps
    std::chrono::milliseconds chronoMillisToNextDraw;

    std::chrono::steady_clock::time_point currentFrameTime;
    QSlider *slider;
    bool drawLast;
    int lastPaintedTick;
    size_t lastDrawingDataTick;

    size_t selectedSimulationSpeed = 10;
    size_t prevSelectedSimulationSpeed = 0; // for pausing
    bool nextSim = false;
    std::chrono::steady_clock::time_point lastFrameSimulated;

    static MainWindow *instance;
    const int TEXT_AREA_WIDTH = 300;
    Ui::MainWindow *ui;

    CoordDrawingHelper *coordDrawingHelper;

    DrawingDataStorage *drawingDataStorage;
    DrawPanel *drawing;
    TextDrawingPanel *textBrowser;
    FpsMeasurer drawingFpsMeasurer = FpsMeasurer(0.1);
    FpsMeasurer strategyFpsMeasurer = FpsMeasurer(0.1);

    std::vector<TickData *> ticksDataStore;
    TickData *currentTickData;
    DrawingData *strategyDrawingData = nullptr;
    model::Game game;

    vector<MyStrategy *> debugStrategies;

    void updatePaintData(int tickNom, DrawingData *drawingData);

    int keyboardModifiersSliderShift(const Qt::KeyboardModifiers &modifiers);

    void prepareTickData(TickData *const tickData, DrawingData *const whereToStore);

    void updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy, TickData *const tickData,
                                              DrawingData *const whereToStore);

    void finishTick(TickData *const tickData, DrawingData *const whereToStore);
};


#endif //CG_MAINWINDOW_H
