//
// Created by dragoon on 17.10.17.
//

#ifndef CPP_CGDK_WORKINGTHREAD_H
#define CPP_CGDK_WORKINGTHREAD_H


#include <Starter.h>
#include <QtCore/QThread>
#include "iostream"

using namespace std;

class WorkingThread : public QThread {

Q_OBJECT

private:
    int argc;
    char **argv;

public:
    WorkingThread(QObject *parent, int argc, char **argv) : QThread(parent), argc(argc), argv(argv) {}

private:
    void run() {
        Starter::runProc(this->argc, this->argv);
    }

};


#endif //CPP_CGDK_WORKINGTHREAD_H
