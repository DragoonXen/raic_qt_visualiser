//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_TICKDATA_H
#define CPP_CGDK_TICKDATA_H


#include <model/PlayerContext.h>

class TickData {
public:
    TickData(const model::PlayerContext &context);

private:
    model::PlayerContext context;

public:
    const model::PlayerContext &getContext() const;
};


#endif //CPP_CGDK_TICKDATA_H
