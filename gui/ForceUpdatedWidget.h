//
// Created by dragoon on 04.11.17.
//

#ifndef CPP_CGDK_FORCEUPDATEDWIDGET_H
#define CPP_CGDK_FORCEUPDATEDWIDGET_H


#include <QtWidgets/QWidget>

class ForceUpdatedWidget : public QWidget {

Q_OBJECT

public:
    ForceUpdatedWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);

    void resetUpdated();

    bool isUpdated() const;

protected:
    void paintEvent(QPaintEvent *event) override;

protected:
    bool updated;

};


#endif //CPP_CGDK_FORCEUPDATEDWIDGET_H
