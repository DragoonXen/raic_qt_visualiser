//
// Created by dragoon on 23.10.17.
//

#ifndef CPP_CGDK_LINE_H
#define CPP_CGDK_LINE_H


#include <QtGui/QPen>
#include "Figure.h"

class Line : public Figure {

private:
    double startX, startY, endX, endY;
    QPen pen;
public:
    Line(double startX, double startY, double endX, double endY, const QPen &pen);

    Line(double startX, double startY, double endX, double endY, const QPen &pen, int priority);

    Line(double startX, double startY, double angle, int length, const QPen &pen);

    Line(double startX, double startY, double angle, int length, const QPen &pen, int priority);

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;

public:
    virtual ~Line();
};


#endif //CPP_CGDK_LINE_H
