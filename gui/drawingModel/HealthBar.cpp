//
// Created by dragoon on 10/22/17.
//

#include "HealthBar.h"

HealthBar::~HealthBar() {
    this->~Figure();
}

void HealthBar::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {

    double upY = cY - size * 0.5 - healthBarHeight * 2;

    double leftX = cX - size * 0.5;

    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    painter.drawConvexPolygon(QRectF(drawingHelper.fixX(leftX),
                                     drawingHelper.fixY(upY),
                                     drawingHelper.fixSize(size),
                                     drawingHelper.fixSize(healthBarHeight)));

    painter.setBrush(Qt::red);
    painter.drawConvexPolygon(QRectF(drawingHelper.fixX(leftX),
                                     drawingHelper.fixY(upY),
                                     drawingHelper.fixSize(size * percentile),
                                     drawingHelper.fixSize(healthBarHeight)));
}

HealthBar::HealthBar(double cX, double cY, int size, float percentile) : Figure(1), cX(cX), cY(cY),
                                                                         percentile(percentile),
                                                                         size(size) {}

HealthBar::HealthBar(double cX, double cY, int size, float percentile, int priority) : Figure(priority), cX(cX), cY(cY),
                                                                                       percentile(percentile),
                                                                                       size(size) {}
