//
// Created by dragoon on 03.11.17.
//

#include "MidColorUtil.h"

QColor MidColorUtil::getColor(QColor argbStart, QColor argbEnd, int minVal, int maxVal, int val) {
    int low = val - minVal;
    if (low == 0) {
        return argbStart;
    }
    int up = maxVal - minVal;
    if (low == up) {
        return argbEnd;
    }
    double percentile = low / (1. * up);
    argbStart.setRed(int(.5 + (argbEnd.red() - argbStart.red()) * percentile + argbStart.red()));
    argbStart.setGreen(int(.5 + (argbEnd.green() - argbStart.green()) * percentile + argbStart.green()));
    argbStart.setBlue(int(.5 + (argbEnd.blue() - argbStart.blue()) * percentile + argbStart.blue()));
    argbStart.setAlpha(int(.5 + (argbEnd.alpha() - argbStart.alpha()) * percentile + argbStart.alpha()));
    return argbStart;
}

QColor MidColorUtil::getColor(QColor argbStart, QColor argbEnd, int maxVal, int val) {
    return getColor(argbStart, argbEnd, 0, maxVal, val);
}

QColor MidColorUtil::getColor(QColor argbStart, QColor argbEnd, double minVal, double maxVal, double val) {
    double low = val - minVal;
    if (low == 0) {
        return argbStart;
    }
    double up = maxVal - minVal;
    if (low == up) {
        return argbEnd;
    }
    double percentile = low / up;
    argbStart.setRed(int(.5 + (argbEnd.red() - argbStart.red()) * percentile + argbStart.red()));
    argbStart.setGreen(int(.5 + (argbEnd.green() - argbStart.green()) * percentile + argbStart.green()));
    argbStart.setBlue(int(.5 + (argbEnd.blue() - argbStart.blue()) * percentile + argbStart.blue()));
    argbStart.setAlpha(int(.5 + (argbEnd.alpha() - argbStart.alpha()) * percentile + argbStart.alpha()));
    return argbStart;
}

QColor MidColorUtil::getColor(QColor argbStart, QColor argbEnd, double maxVal, double val) {
    return getColor(argbStart, argbEnd, 0., maxVal, val);
}
