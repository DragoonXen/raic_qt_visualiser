//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_FIGURE_H
#define CPP_CGDK_FIGURE_H


#include <gui/CoordDrawingHelper.h>
#include <QtGui/QPainter>

class Figure {

private:
    int priority;
public:
    Figure() : priority(0) {};

    Figure(int priority) : priority(priority) {}

    virtual void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const = 0;

    int getPriority() const;

    virtual ~Figure();
};


#endif //CPP_CGDK_FIGURE_H
