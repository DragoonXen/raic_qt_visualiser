//
// Created by dragoon on 03.11.17.
//

#ifndef CPP_CGDK_MIDCOLORUTIL_H
#define CPP_CGDK_MIDCOLORUTIL_H


#include <QtGui/QColor>

class MidColorUtil {
public:
    static QColor getColor(QColor argbStart, QColor argbEnd, int minVal, int maxVal, int val);

    static QColor getColor(QColor argbStart, QColor argbEnd, int maxVal, int val);

    static QColor getColor(QColor argbStart, QColor argbEnd, double minVal, double maxVal, double val);

    static QColor getColor(QColor argbStart, QColor argbEnd, double maxVal, double val);
};


#endif //CPP_CGDK_MIDCOLORUTIL_H
