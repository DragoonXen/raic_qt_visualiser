//
// Created by dragoon on 03.11.17.
//

#ifndef CPP_CGDK_SQUARE_H
#define CPP_CGDK_SQUARE_H


#include <bits/unique_ptr.h>
#include "Figure.h"

class Rect : public Figure {

private:
    double x, y, width, height;
    QBrush brush;

public:
    Rect(double x, double y, double width, double height, QBrush brush);

    Rect(double x, double y, double width, double height, QBrush brush, int priority);

    static std::unique_ptr<Rect> createRect(double x, double y, double width, double height, QBrush brush);

    static std::unique_ptr<Rect>
    createRect(double x, double y, double width, double height, QBrush brush, int priority);

    static std::unique_ptr<Rect> createRectByCenter(double cX, double cY, double width, double height, QBrush brush);

    static std::unique_ptr<Rect>
    createRectByCenter(double cX, double cY, double width, double height, QBrush brush, int priority);

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;
};


#endif //CPP_CGDK_SQUARE_H
