//
// Created by dragoon on 23.10.17.
//

#include "Line.h"

Line::~Line() {

}

Line::Line(double startX, double startY, double endX, double endY, const QPen &pen) : startX(startX), startY(startY),
                                                                                      endX(endX), endY(endY),
                                                                                      pen(pen) {}

Line::Line(double startX, double startY, double endX, double endY, const QPen &pen, int priority) : Figure(priority),
                                                                                                    startX(startX),
                                                                                                    startY(startY),
                                                                                                    endX(endX),
                                                                                                    endY(endY),
                                                                                                    pen(pen) {}

Line::Line(double startX, double startY, double angle, int length, const QPen &pen) : startX(startX), startY(startY),
                                                                                      pen(pen) {
    endX = startX + length * cos(angle);
    endY = startY + length * sin(angle);
}

Line::Line(double startX, double startY, double angle, int length, const QPen &pen, int priority) : Figure(priority),
                                                                                                    startX(startX),
                                                                                                    startY(startY),
                                                                                                    pen(pen) {
    endX = startX + length * cos(angle);
    endY = startY + length * sin(angle);
}

void Line::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {
    painter.setPen(pen);
    painter.drawLine(QLineF(drawingHelper.fixX(startX),
                            drawingHelper.fixY(startY),
                            drawingHelper.fixX(endX),
                            drawingHelper.fixY(endY)));
}
