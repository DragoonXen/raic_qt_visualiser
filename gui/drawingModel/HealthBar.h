//
// Created by dragoon on 10/22/17.
//

#ifndef CPP_CGDK_HEALTHBAR_H
#define CPP_CGDK_HEALTHBAR_H


#include "Figure.h"

class HealthBar : public Figure {

private:
    static constexpr int healthBarHeight = 5;
    double cX, cY;
    float percentile;
    int size;
public:
    HealthBar(double cX, double cY, int size, float percentile);

    HealthBar(double cX, double cY, int size, float percentile, int priority);

    ~HealthBar() override;

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;
};


#endif //CPP_CGDK_HEALTHBAR_H
