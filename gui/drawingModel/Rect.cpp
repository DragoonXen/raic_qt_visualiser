//
// Created by dragoon on 03.11.17.
//

#include "Rect.h"

Rect::Rect(double x, double y, double width, double height, QBrush brush) : x(x),
                                                                            y(y),
                                                                            width(width),
                                                                            height(height),
                                                                            brush(brush) {}

Rect::Rect(double x, double y, double width, double height, QBrush brush, int priority) : Figure(priority),
                                                                                          x(x),
                                                                                          y(y),
                                                                                          width(width),
                                                                                          height(height),
                                                                                          brush(brush) {}

void Rect::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {
    painter.setPen(Qt::NoPen);
    painter.setBrush(brush);
    painter.drawRect(QRectF(drawingHelper.fixX(x),
                            drawingHelper.fixY(y),
                            drawingHelper.fixSize(width),
                            drawingHelper.fixSize(height)));

}

std::unique_ptr<Rect> Rect::createRect(double x, double y, double width, double height, QBrush brush) {
    return std::make_unique<Rect>(x, y, width, height, brush);
}

std::unique_ptr<Rect> Rect::createRect(double x, double y, double width, double height, QBrush brush, int priority) {
    return std::make_unique<Rect>(x, y, width, height, brush, priority);
}

std::unique_ptr<Rect> Rect::createRectByCenter(double cX, double cY, double width, double height, QBrush brush) {
    return std::make_unique<Rect>(cX - width / 2., cY - height / 2., width, height, brush);
}

std::unique_ptr<Rect>
Rect::createRectByCenter(double cX, double cY, double width, double height, QBrush brush, int priority) {
    return std::make_unique<Rect>(cX - width / 2., cY - height / 2., width, height, brush, priority);
}
