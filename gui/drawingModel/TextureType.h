//
// Created by dragoon on 25.10.17.
//

#ifndef CPP_CGDK_TEXTURETYPE_H
#define CPP_CGDK_TEXTURETYPE_H

enum TextureType {
    Heli, Plane, Tank, BMP, BREM
};

#endif //CPP_CGDK_TEXTURETYPE_H
