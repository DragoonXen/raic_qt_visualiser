//
// Created by dragoon on 10/21/17.
//

#include <iostream>
#include "DrawingData.h"

DrawingData::DrawingData(TickData *tickData) : tickData(tickData) {}

void DrawingData::drawAll(QPainter &painter, const CoordDrawingHelper &drawingHelper) {
    std::sort(figures.begin(), figures.end(),
              [](const std::unique_ptr<Figure> &lhs, const std::unique_ptr<Figure> &rhs) { return lhs->getPriority() < rhs->getPriority(); });
    for (auto &figure : figures) {
        figure->paintFigure(painter, drawingHelper);
    }
}

void DrawingData::addFigure(std::unique_ptr<Figure> figure) {
    this->figures.emplace_back(std::move(figure));
}

void DrawingData::putLine(std::string line, size_t position) {
    while (lines.size() <= position) {
        lines.push_back("");
    }
    lines[position] = line;
}

const TickData *DrawingData::getTickData() const {
    return tickData;
}

std::vector<std::string> *DrawingData::getLines() {
    return &lines;
}
