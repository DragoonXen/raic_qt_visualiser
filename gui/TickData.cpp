//
// Created by dragoon on 10/21/17.
//

#include "TickData.h"

TickData::TickData(const model::PlayerContext &context) : context(context) {}

const model::PlayerContext &TickData::getContext() const {
    return context;
}
