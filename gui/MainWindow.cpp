//
// Created by dragoon on 04.08.17.
//

#include <ui_MainWindow.h>
#include "MainWindow.h"
#include <QtWidgets>
#include <gui/drawingModel/Circle.h>
#include <chrono>
#include <gui/drawingModel/HealthBar.h>
#include <gui/drawingModel/Line.h>
#include <gui/drawingModel/TexturePainter.h>
#include <gui/drawingModel/Rect.h>
#include <gui/drawingModel/MidColorUtil.h>
#include <thread>
#include <sstream>
#include <iomanip>

using namespace std;

MainWindow *MainWindow::instance;

const std::array<int, 11> MainWindow::framesPerSecond = {0, 1, 5, 10, 15, 30, 45, 60, 90, 120, 1000};

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent), ui(new Ui::MainWindow) {

    millisToNextDraw = 1000 / 30; // 30 fps
    chronoMillisToNextDraw = std::chrono::milliseconds(millisToNextDraw);

    ui->setupUi(this);
    setMinimumSize(500, 300);

    this->drawingDataStorage = new DrawingDataStorage;
    this->drawing = new DrawPanel(findChild<QWidget *>("centralWidget"), this->drawingDataStorage);
    this->drawing->setObjectName(QStringLiteral("drawPanel"));
    this->drawing->setGeometry(QRect(10, 10, 256, 192));

    this->textBrowser = new TextDrawingPanel(findChild<QWidget *>("centralWidget"), this->drawingDataStorage);
    this->textBrowser->setObjectName(QStringLiteral("textBrowser"));
    this->textBrowser->setGeometry(QRect(270, 10, 150, 192));
    this->textBrowser->setStyleSheet("background-color:white;");

    MainWindow::instance = this;

    slider = findChild<QSlider *>("horizontalSlider");
    slider->setMinimum(0);
    slider->setMaximum(-1);
    slider->setValue(0);

    QObject::connect(slider, SIGNAL(valueChanged(int)),
                     this, SLOT(changeDrawingTick(int)),
                     Qt::ConnectionType::DirectConnection);

    this->drawLast = true;
    this->lastPaintedTick = -2;
    grabKeyboard();

    QTimer::singleShot(millisToNextDraw, Qt::TimerType::CoarseTimer, this, SLOT(runRepaint()));
    currentFrameTime = std::chrono::steady_clock::now() + std::chrono::milliseconds(millisToNextDraw);
    lastFrameSimulated = std::chrono::steady_clock::now();

    coordDrawingHelper = new CoordDrawingHelper(0, 0, 4000, 4000, this->drawing->geometry().width(),
                                                this->drawing->geometry().height());
    TexturePainter::initTextures();
}

void MainWindow::initConstants(model::Game game, size_t teamSize) {
    this->game = game;
    CoordDrawingHelper *newCoordDrawingHelper = new CoordDrawingHelper(0,
                                                                       0,
                                                                       game.getMapSize(),
                                                                       game.getMapSize(),
                                                                       this->drawing->geometry().width(),
                                                                       this->drawing->geometry().height());
    CoordDrawingHelper *bkp = coordDrawingHelper;
    coordDrawingHelper = newCoordDrawingHelper;
    delete bkp;

    for (size_t i = 0; i != teamSize; ++i) {
        this->debugStrategies.push_back(new MyStrategy());
    }
}


void MainWindow::resizeEvent(QResizeEvent *evt) {
    this->lastPaintedTick = -2;
//    cout << size().width() << ' ' << size().height() << endl;

    {
        const QRect sliderRect = slider->geometry();
        int sliderY = size().height() - 50;
        slider->setGeometry(sliderRect.x(), sliderY, size().width() - 20, sliderRect.height());
        slider->updateGeometry();

        const QRect textRect = textBrowser->geometry();
        textBrowser->setGeometry(size().width() - TEXT_AREA_WIDTH, textRect.y(), TEXT_AREA_WIDTH - 5,
                                 sliderY - textRect.y());
        textBrowser->updateGeometry();

        const QRect drawingRect = this->drawing->geometry();
        this->drawing->setGeometry(drawingRect.x(), drawingRect.y(),
                                   size().width() - TEXT_AREA_WIDTH - 5 - drawingRect.x(),
                                   sliderY - drawingRect.y());
        this->drawing->updateGeometry();
        QRect sceneRect = this->drawing->rect();
        sceneRect.setWidth(sceneRect.width() - 2);
        sceneRect.setHeight(sceneRect.height() - 2);
        this->coordDrawingHelper->resizeDrawingWindow(sceneRect.width(), sceneRect.height());
    }
}


MainWindow::~MainWindow() {
    delete ui;
}

template<typename... Args>
std::string format(const char *fmt, Args... args) {
    char buf[100];
    snprintf(buf, 100, fmt, args...);
    return buf;
}

void MainWindow::updatePaintData(int tickNom, DrawingData *drawingData) {
    if (drawingData != nullptr) {
        drawingData->putLine(std::to_string(tickNom) + "/" + std::to_string(slider->maximum()), 0);
        int designedFps = framesPerSecond[selectedSimulationSpeed];
        double drawingFps = this->drawingFpsMeasurer.getFps();
        double strategyFps = this->strategyFpsMeasurer.getFps();
        drawingData->putLine(format("process: %d, strategy: %.2f\ndrawing: %.2f", designedFps, strategyFps, drawingFps),
                             1);
    }
//    textBrowser->setHtml(stringLinesHtml->getAsHtml());
    lastPaintedTick = tickNom;
    lastDrawingDataTick = this->ticksDataStore.size();
}

MainWindow *MainWindow::getInstance() {
    return MainWindow::instance;
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    std::cout << "key pressed" << std::endl;
    switch (event->key()) {
        case Qt::Key_Plus:
            if (selectedSimulationSpeed + 1 < framesPerSecond.size()) {
                ++selectedSimulationSpeed;
            }
            break;
        case Qt::Key_Minus:
            if (selectedSimulationSpeed > 0) {
                --selectedSimulationSpeed;
            }
            break;
        case Qt::Key_Space:
            if (this->selectedSimulationSpeed == 0) {
                this->selectedSimulationSpeed = this->prevSelectedSimulationSpeed;
            } else {
                this->prevSelectedSimulationSpeed = this->selectedSimulationSpeed;
                this->selectedSimulationSpeed = 0;
            }
            break;
        case ']':
            if (!this->drawLast) {
                this->drawLast = true;
                changeDrawingTick(this->slider->maximum());
            }
            break;
        case Qt::Key_Less:
        case Qt::Key_Comma:
            this->drawLast = false;
            if (this->slider->value() > 0) {
                int shift = keyboardModifiersSliderShift(event->modifiers());
                changeDrawingTick(this->slider->value() - shift);
            }
            break;
        case Qt::Key_Greater:
        case Qt::Key_Period:
            if (drawLast && selectedSimulationSpeed == 0) {
                nextSim = true;
            } else {
                this->drawLast = false;
                if (this->slider->value() < this->slider->maximum()) {
                    int shift = keyboardModifiersSliderShift(event->modifiers());
                    changeDrawingTick(this->slider->value() + shift);
                }
            }
            break;
        case Qt::Key_P:
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Up:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::UP);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Down:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::DOWN);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Left:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::LEFT);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Right:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::RIGHT);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Enter:
            int lastPaintedTick = this->lastPaintedTick;
            this->lastPaintedTick = -2;
            changeDrawingTick(lastPaintedTick);
            break;
    }
    QWidget::keyPressEvent(event);
}

void MainWindow::changeDrawingTick(int changedValue) {
    this->slider->blockSignals(true);
    this->slider->setValue(changedValue);
    this->slider->blockSignals(false);
    if (this->drawLast && changedValue != this->slider->maximum()) {
        this->drawLast = false;
    }
    if (changedValue < 0) {
        changedValue = 0;
    } else if (changedValue > this->slider->maximum()) {
        changedValue = this->slider->maximum();
    }

    TickData *debugTickData = this->ticksDataStore[changedValue];
    DrawingData *debugDrawingData = new DrawingData(debugTickData);
    if (debugTickData == nullptr) {
        finishTick(debugTickData, debugDrawingData);
        return;
    }

    prepareTickData(debugTickData, debugDrawingData);

    auto &context = debugTickData->getContext();
    auto &playerWizards = context.getWizards();
    if (playerWizards.size() != this->debugStrategies.size()) {
        return;
    }
    for (size_t wizardIndex = 0; wizardIndex < this->debugStrategies.size(); ++wizardIndex) {
        auto &playerWizard = playerWizards[wizardIndex];

        model::Move move;
        this->debugStrategies[wizardIndex]->move(playerWizard, context.getWorld(), game, move);
    }

    for (auto &strategy : this->debugStrategies) {
        MainWindow::getInstance()->updateCalculatedStrategyDrawDataTick(*strategy, debugTickData, debugDrawingData);
    }
    finishTick(debugTickData, debugDrawingData);
}

int MainWindow::keyboardModifiersSliderShift(const Qt::KeyboardModifiers &modifiers) {
    if (modifiers.testFlag(Qt::KeyboardModifier::ShiftModifier)) {
        return 100;
    }
    if (modifiers.testFlag(Qt::KeyboardModifier::ControlModifier)) {
        return 10;
    }
    return 1;
}

void MainWindow::runRepaint() {
    this->drawingFpsMeasurer.putTime();
    this->drawingDataStorage->selectAndCleanDrawingData();
    auto drawingData = this->drawingDataStorage->getCurrentDrawingData();
    int tickNom = slider->value();
    if (drawingData != nullptr && drawingData->getTickData() != nullptr) {
        tickNom = drawingData->getTickData()->getContext().getWorld().getTickIndex();
    }
    if (tickNom != this->lastPaintedTick) {
        updatePaintData(tickNom, drawingData);
        this->drawing->resetUpdated();
        this->textBrowser->resetUpdated();
        this->drawing->updateHelperData(this->coordDrawingHelper);

        this->centralWidget()->repaint();
        if (!this->drawing->isUpdated()) {
            std::cout << "force drawing repaint" << std::endl;
            this->drawing->repaint();
        }
        if (!this->textBrowser->isUpdated()) {
            std::cout << "force textBrowser repaint" << std::endl;
            this->textBrowser->repaint();
        }
    } else {
        updatePaintData(tickNom, drawingData);
        this->textBrowser->repaint();
    }
    currentFrameTime += chronoMillisToNextDraw;
    std::chrono::steady_clock::time_point current = std::chrono::steady_clock::now();
    int timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(currentFrameTime - current).count();
    if (timeDiff < 1) {
        qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        timeDiff = 1;
//        currentFrameTime = current + 1ms;
        currentFrameTime = std::chrono::steady_clock::now() + 1ms;
    }
    QTimer::singleShot(timeDiff, Qt::TimerType::CoarseTimer, this, SLOT(runRepaint()));
}

void MainWindow::wheelEvent(QWheelEvent *event) {
    if (this->drawing->geometry().contains(event->pos())) {
        const QRect &rect = this->drawing->geometry();
        double rollX = (event->x() - rect.x()) / (rect.width() * 1.);
        double rollY = (event->y() - rect.y()) / (rect.height() * 1.);
        if (event->delta() > 0) {
            this->coordDrawingHelper->zoomIn(rollX, rollY);
        } else {
            this->coordDrawingHelper->zoomOut(rollX, rollY);
        }
        this->lastPaintedTick = -2;
    } else {
        QWidget::wheelEvent(event);
    }
}

void MainWindow::prepareTickData(const model::PlayerContext &context) {
    this->currentTickData = new TickData(context);
    this->strategyDrawingData = new DrawingData(this->currentTickData);

    prepareTickData(this->currentTickData, this->strategyDrawingData);
}

void MainWindow::updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy) {
    updateCalculatedStrategyDrawDataTick(strategy, this->currentTickData, this->strategyDrawingData);
}

void MainWindow::finishTick() {
    strategyFpsMeasurer.putTime();
    while (this->currentTickData->getContext().getWorld().getTickIndex() != this->ticksDataStore.size()) {
        this->ticksDataStore.push_back(nullptr);
    }
    this->ticksDataStore.push_back(this->currentTickData);
    this->slider->blockSignals(true);
    this->slider->setRange(0, ticksDataStore.size() - 1);
    this->slider->blockSignals(false);

    if (drawLast) {
        this->slider->blockSignals(true);
        this->slider->setValue(ticksDataStore.size() - 1);
        this->slider->blockSignals(false);
        finishTick(this->currentTickData, this->strategyDrawingData);
    } else {
        delete this->strategyDrawingData;
    }

    int designedFps = framesPerSecond[selectedSimulationSpeed];
    do {
        while (designedFps == 0) {
            std::this_thread::sleep_for(10ms);
            if (nextSim) {
                nextSim = false;
                return;
            }
            designedFps = framesPerSecond[selectedSimulationSpeed];
        }
        int millistToNextDraw = 1000 / designedFps;
        lastFrameSimulated += std::chrono::milliseconds(millistToNextDraw);
        std::chrono::steady_clock::time_point current = std::chrono::steady_clock::now();
        auto timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(lastFrameSimulated - current);
        if (timeDiff.count() > 0) {
            std::this_thread::sleep_for(timeDiff);
            designedFps = framesPerSecond[selectedSimulationSpeed];
        } else {
            lastFrameSimulated = current;
        }
    } while (designedFps == 0);
}

void MainWindow::prepareTickData(TickData *const tickData, DrawingData *const drawingData) {
    for (const auto &tree : tickData->getContext().getWorld().getTrees()) {
        drawingData->addFigure(std::make_unique<Circle>(tree.getX(), tree.getY(), tree.getRadius(), QBrush(Qt::green)));
    }

    drawingData->addFigure(std::make_unique<Line>(4000., 0., 0., 4000., QPen(Qt::red), -1000));

//    drawingData->addFigure(std::make_unique<Circle>(10., 10., 5., QBrush(Qt::black), 0));
//    drawingData->addFigure(std::make_unique<Circle>(10., 20., 5., QBrush(Qt::darkGreen), 1));
//    drawingData->addFigure(std::make_unique<Circle>(20., 20., 5., QBrush(Qt::black), QPen(Qt::red), 2));
//    drawingData->addFigure(std::make_unique<Circle>(20., 10., 5., QBrush(Qt::darkGreen), QPen(Qt::magenta), 3));

    const auto &myFaction = tickData->getContext().getWorld().getMyPlayer().getFaction();
//    bool inited = false;
    for (const auto &minion : tickData->getContext().getWorld().getMinions()) {
        drawingData->addFigure(
                std::make_unique<Circle>(minion.getX(), minion.getY(), minion.getRadius(),
                                         QBrush(minion.getFaction() == myFaction ? Qt::green : Qt::red)));
        drawingData->addFigure(std::make_unique<HealthBar>(minion.getX(), minion.getY(), minion.getRadius() * 2,
                                                           minion.getLife() * 1. / minion.getMaxLife()));
        drawingData->addFigure(std::make_unique<TexturePainter>(minion.getX(), minion.getY(),
                                                                minion.getType() ==
                                                                model::MinionType::MINION_FETISH_BLOWDART ?
                                                                TextureType::Heli : TextureType::Tank,
                                                                minion.getRadius() * 2));
//        if (!inited) {
//            inited = true;
//            for (int i = 0; i != 5000; ++i) {
//                int x = std::rand() % int(4000 - minion.getRadius() * 2) + minion.getRadius();
//                int y = std::rand() % int(4000 - minion.getRadius() * 2) + minion.getRadius();
//                drawingData->addFigure(
//                        std::make_unique<Circle>(x, y, minion.getRadius(),
//                                                 QBrush(rand() & 1 ? Qt::green : Qt::red)));
//                drawingData->addFigure(std::make_unique<HealthBar>(x, y, minion.getRadius() * 2,
//                                                                   (rand() % minion.getMaxLife()) * 1. /
//                                                                   minion.getMaxLife()));
//                drawingData->addFigure(std::make_unique<TexturePainter>(x, y,
//                                                                        (rand() & 1) ?
//                                                                        TextureType::Heli : TextureType::Tank,
//                                                                        minion.getRadius() * 2));
//            }
//        }
    }


    for (const auto &building : tickData->getContext().getWorld().getBuildings()) {
        drawingData->addFigure(
                std::make_unique<Circle>(building.getX(),
                                         building.getY(),
                                         building.getRadius(),
                                         QBrush(building.getFaction() == myFaction ? Qt::green : Qt::red)));
        drawingData->addFigure(std::make_unique<HealthBar>(building.getX(), building.getY(), building.getRadius() * 2,
                                                           building.getLife() * 1. / building.getMaxLife()));
        drawingData->addFigure(std::make_unique<TexturePainter>(building.getX(), building.getY(),
                                                                building.getType() ==
                                                                model::BuildingType::BUILDING_GUARDIAN_TOWER ?
                                                                TextureType::BMP : TextureType::BREM,
                                                                building.getRadius() * 2));

    }

    for (const auto &wizard : tickData->getContext().getWorld().getWizards()) {
        drawingData->addFigure(
                std::make_unique<Circle>(wizard.getX(),
                                         wizard.getY(),
                                         wizard.getRadius(),
                                         QBrush(wizard.getFaction() == myFaction ? Qt::green : Qt::red)));
        drawingData->addFigure(std::make_unique<HealthBar>(wizard.getX(), wizard.getY(), wizard.getRadius() * 2,
                                                           wizard.getLife() * 1. / wizard.getMaxLife()));

        drawingData->addFigure(
                std::make_unique<Line>(wizard.getX(), wizard.getY(), wizard.getAngle(), ((int) wizard.getRadius()) + 10,
                                       QPen(Qt::black), 1));
        drawingData->addFigure(std::make_unique<TexturePainter>(wizard.getX(), wizard.getY(), TextureType::Plane,
                                                                wizard.getRadius() * 2));

    }

    {
        int size = 4000 / 20;
        QColor start = QColor(255, 0, 0, 40);
        QColor end = QColor(0, 255, 0, 40);
        srand(tickData->getContext().getWorld().getTickIndex());
        for (int i = 0; i != 20; ++i) {
            int sX = i * 4000 / 20;
            for (int j = 0; j != 20; ++j) {
                int sY = j * 4000 / 20;
                drawingData->addFigure(Rect::createRect(sX, sY, size, size,
                                                        MidColorUtil::getColor(start, end, 99, rand() % 100),
                                                        -1000));
            }
        }
    }
}

void MainWindow::updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy, TickData *const tickData,
                                                      DrawingData *const drawingData) {

}

void MainWindow::finishTick(TickData *const tickData, DrawingData *const drawingData) {
    this->drawingDataStorage->updateDrawingData(drawingData);
}
