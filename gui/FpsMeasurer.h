//
// Created by dragoon on 03.11.17.
//

#ifndef CPP_CGDK_FPSMEASURER_H
#define CPP_CGDK_FPSMEASURER_H


#include <chrono>

class FpsMeasurer {

private:
    double newMeasureWeiht;
    double currentFps;
    std::chrono::steady_clock::time_point prevTime;

public:
    FpsMeasurer(double newMeasureWeiht);

    void putTime();

    double getFps();
};


#endif //CPP_CGDK_FPSMEASURER_H
