//
// Created by dragoon on 03.11.17.
//

#include "FpsMeasurer.h"

FpsMeasurer::FpsMeasurer(double newMeasureWeiht) : newMeasureWeiht(newMeasureWeiht) {
    prevTime = std::chrono::steady_clock::now();
    this->currentFps = 0;
}

void FpsMeasurer::putTime() {
    auto now = std::chrono::steady_clock::now();
    double newVal = 1e6 / std::chrono::duration_cast<std::chrono::microseconds>(now - prevTime).count();
    this->currentFps = this->currentFps * (1 - newMeasureWeiht) + newMeasureWeiht * newVal;
    prevTime = now;
}

double FpsMeasurer::getFps() {
    return this->currentFps;
}
