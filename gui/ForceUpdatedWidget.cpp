//
// Created by dragoon on 04.11.17.
//

#include "ForceUpdatedWidget.h"


ForceUpdatedWidget::ForceUpdatedWidget(QWidget *parent, Qt::WindowFlags f) : QWidget(parent, f) {
}

bool ForceUpdatedWidget::isUpdated() const {
    return updated;
}

void ForceUpdatedWidget::resetUpdated() {
    this->updated = false;
}

void ForceUpdatedWidget::paintEvent(QPaintEvent *event) {
    this->updated = true;
    QWidget::paintEvent(event);
}
