//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_STARTER_H
#define CPP_CGDK_STARTER_H


class Starter {
public:
    static int runProc(int argc, char **argv);
};

#endif //CPP_CGDK_STARTER_H
