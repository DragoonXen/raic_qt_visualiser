#pragma once

#ifndef _MY_STRATEGY_H_
#define _MY_STRATEGY_H_

#include "Strategy.h"
#include <iostream>

#ifdef WITH_VISUALIZER

#include <QtCore/QtCore>

#endif

using namespace std;

class MyStrategy : public Strategy {
public:
    MyStrategy();

    void move(const model::Wizard &self, const model::World &world, const model::Game &game, model::Move &move);
};

#ifdef WITH_VISUALIZER

Q_DECLARE_METATYPE(MyStrategy);
#endif

#endif
