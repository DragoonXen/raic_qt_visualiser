#include "Runner.h"

#ifdef WITH_VISUALIZER

#include <gui/MainWindow.h>
#include <QtWidgets/QApplication>
#include <gui/WorkingThread.h>

#endif

#include "MyStrategy.h"
#include "Starter.h"

using namespace model;
using namespace std;

int main(int argc, char *argv[]) {
#ifdef WITH_VISUALIZER
    QApplication *a = new QApplication(argc, argv);
    MainWindow *w = new MainWindow();
    w->show();
    WorkingThread wt(0, argc, argv);
    wt.start();
    return a->exec();
#else
    return Starter::runProc(argc, argv);
#endif
}

Runner::Runner(const char *host, const char *port, const char *token)
        : remoteProcessClient(host, atoi(port)), token(token) {
}

void Runner::run() {
    if (!remoteProcessClient.inited) {
        return;
    }
    try {
        remoteProcessClient.writeTokenMessage(token);
        remoteProcessClient.writeProtocolVersionMessage();
        size_t teamSize = remoteProcessClient.readTeamSizeMessage();
        Game game = remoteProcessClient.readGameContextMessage();
#ifdef WITH_VISUALIZER
        MainWindow::getInstance()->initConstants(game, teamSize);
#endif

        vector<Strategy *> strategies;

        for (size_t strategyIndex = 0; strategyIndex < teamSize; ++strategyIndex) {
            Strategy *strategy = new MyStrategy;
            strategies.push_back(strategy);
        }

        PlayerContext *playerContext;
        cout << "started" << endl;
        while ((playerContext = remoteProcessClient.readPlayerContextMessage()) != nullptr) {
            vector<Wizard> playerWizards = playerContext->getWizards();
            if (playerWizards.size() != teamSize) {
                break;
            }
            cout << "tick #" << playerContext->getWorld().getTickIndex() << endl;

#ifdef WITH_VISUALIZER
            vector<Move> moves;

            MainWindow::getInstance()->prepareTickData(*playerContext);

            for (size_t wizardIndex = 0; wizardIndex < teamSize; ++wizardIndex) {
                Wizard playerWizard = playerWizards[wizardIndex];

                Move move;
                strategies[wizardIndex]->move(playerWizard, playerContext->getWorld(), game, move);
                moves.push_back(move);
            }

            for (size_t wizardIndex = 0; wizardIndex < teamSize; ++wizardIndex) {
                MainWindow::getInstance()->updateCalculatedStrategyDrawDataTick((MyStrategy &) *strategies[wizardIndex]);
            }
            MainWindow::getInstance()->finishTick();

            remoteProcessClient.writeMovesMessage(moves);
#else
            vector<Move> moves;

            for (size_t wizardIndex = 0; wizardIndex < teamSize; ++wizardIndex) {
                Wizard playerWizard = playerWizards[wizardIndex];

                Move move;
                strategies[wizardIndex]->move(playerWizard, playerContext->getWorld(), game, move);
                moves.push_back(move);
            }

            remoteProcessClient.writeMovesMessage(moves);
#endif

            delete playerContext;
        }

        for (size_t strategyIndex = 0; strategyIndex < teamSize; ++strategyIndex) {
            delete strategies[strategyIndex];
        }
    } catch (...) {
        cout << "oops, smth failed!" << endl;
    }
}
